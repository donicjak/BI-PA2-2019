# Editor MAKEFILE
# make all        Builds the package + documentation
# make run        Builds and runs the program
# make clean      Cleans results of building process and documentation
# make doc        Generates the documentation with doxygen
# make compile    Builds the program.

CC=g++
CFLAGS=-Wall -pedantic -Wno-long-long -O0 -ggdb -std=c++11  
DOXY=Doxyfile

CPPDIR=./src/cpp/
HEADERDIR=./src/header/
OBJDIR=./src/object/

EXEC=donicjak

OBJFILES=$(OBJDIR)main.o $(OBJDIR)Initialize.o $(OBJDIR)Menu.o $(OBJDIR)Note.o $(OBJDIR)Memory.o $(OBJDIR)FileSaver.o $(OBJDIR)Window.o $(OBJDIR)Find.o -lncurses -lmenu

$(EXEC): $(OBJFILES)
			   $(CC) $(OBJFILES) -o $(EXEC)

$(OBJDIR)Initialize.o: $(CPPDIR)Initialize.cpp $(HEADERDIR)Initialize.h
				$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)main.o: $(CPPDIR)main.cpp $(HEADERDIR)Initialize.h $(HEADERDIR)Menu.h
				$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)Menu.o: $(CPPDIR)Menu.cpp $(HEADERDIR)Menu.h $(HEADERDIR)Initialize.h $(HEADERDIR)Find.h $(HEADERDIR)Window.h
				$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)Window.o: $(CPPDIR)Window.cpp $(HEADERDIR)Window.h $(HEADERDIR)Note.h
				$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)FileSaver.o: $(CPPDIR)FileSaver.cpp $(HEADERDIR)FileSaver.h $(HEADERDIR)Note.h
				$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)Note.o: $(CPPDIR)Note.cpp $(HEADERDIR)Note.h $(HEADERDIR)Memory.h $(HEADERDIR)Menu.h
				$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)Memory.o: $(CPPDIR)Memory.cpp $(HEADERDIR)Memory.h
				$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)Find.o: $(CPPDIR)Find.cpp $(HEADERDIR)Find.h $(HEADERDIR)Window.h
				$(CC) $(CFLAGS) -c $< -o $@

all:
	make compile
	make doc

clean:
	rm $(OBJDIR)/* $(EXEC)
	rm -rf ./doc

compile: $(EXEC)
	
doc:
	doxygen $(DOXY)	
	
run:
	./$(EXEC)

