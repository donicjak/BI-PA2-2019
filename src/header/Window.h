#ifndef Window_H_DEFINED
#define Window_H_DEFINED

#include <ncurses.h>
#include <string>
#include <iostream>
#include <vector>
#include "../header/Note.h" 


using namespace std;

// Class to initialize and end screen for editor.

class Window 
{
    public:
    static void init( string filename, const int & format   );

    private:
    static void start_editor( string  filename, const int & format );
    static void end_scr ();
    
};


#endif //Window_H_DEFINED
