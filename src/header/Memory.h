#ifndef Memory_H_DEFINED
#define Memory_H_DEFINED

#include <vector>
#include <string>

using namespace std;

/**
 * This class is represing buffer of memory
 * It will represent lines of text in a vector format
 * Providing methods to add, delete and append lines.
 * */

class Memory
{

    public:
    Memory();
    vector < string > text_lines; // vector of lines
    void in_Line ( string line, const int & nr ); // inserting a line
    void rem_Line ( const int & nr  ); // removing a line
    void app_Line ( string line ); // appending a line
    string rem_Tabs( string line ); // removing tab

};


#endif //Memory_H_DEFINED