#ifndef NOTEDATA_H
#define NOTEDATA_H

#include <string>
#include <vector>

/**
 * Class representing data of a single Note
 * */

class NoteData
{
public:

    NoteData(); // constructor

    virtual ~NoteData(); // destructor

    std::string getCategory() const
    {
        return category_;   // gets a category
    }

    void setCategory(std::string category)
    {
        this->category_ = category_; // sets a category
    }

    std::vector<std::string> getTags() const
    {
        return tags_; // gets a tags
    }

    void setTags(std::vector<std::string> tags)
    {
        this->tags_ = tags_; // sets a tags
    }
    
    void setText_lines(std::vector<std::string> text_lines)
    {
        this->text_lines_ = text_lines; // inputs a line of text
    }

    std::vector<std::string> getText_lines() const
    {
        return text_lines_; // gets a line of text
    }

    std::string getTitle() const
    {
        return title_;
    }

    void setTitle(std::string title_)
    {
        this->title_ = title_;
    }
    
    void removeLine(int lineNumber); // removes a line
    
    void apendLine(std::string line); //appends a line
    
    void insertLine(std::string line, int lineNumber); // inserts a line
    
private:
    std::string title_;
    std::string category_;

    std::vector < std::string > text_lines_; // represnet all text line

    std::vector < std::string > tags_; // vector of string representing all tags
    
    std::string removeTabs(std::string line);
};

#endif /* NOTEDATA_H */

