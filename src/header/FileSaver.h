#ifndef FILESAVER_H
#define FILESAVER_H

#include <fstream>

class Note;

// class created for saving a file

class FileSaver
{
public:
    FileSaver(std::string baseDirectoryPath, std::string fileNameExtension); // constructor with filename and destination
    
    void saveNoteToFile(Note& noteToSave, std::string& fileName); // method for savinga file.
    
    virtual ~FileSaver(); // basic destructor
private:
    std::string baseDirectoryPath_;
    std::string fileNameExtension_;
    
};

#endif /* FILESAVER_H */

