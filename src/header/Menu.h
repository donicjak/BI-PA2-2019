#ifndef Menu_H_DEFINED
#define Menu_H_DEFINED

#include <ncurses.h>
#include <iostream>
#include <string>

using namespace std;

/**
 * Namespace made fot intializiting
 * and displaying a basic menu
 * and different choice options in it.
 * */

namespace Menu 
{
    void init_main();     // method for initializing main menu 
    void init_create(); // method for initializing create menu, which is called after user selects create/open box in main menu
    void init_search(); // method for initializing search menu, which is called after user selects search box in main menu
    string get_String(); // method for reading input from a user into a string
    void list_files( const int & format ); // method which displays all files present in directory base on format user selects
    void clean_win( const int & format ); // method for initializing empty screen based on format user sets
    void clean_win( ); // method for initializing empty screen
    bool has_suffix(const std::string &str, const std::string &suffix); // method which return true if string contains set suffix (=ending)
    void select_main ( int highlight ); // helping method for handeling user input when in main menu
    void select_create ( int highlight ); // helping method for handeling user input when in create menu
    void select_search ( int highlight ); // helping method for handeling user input when in search menu
}

#endif //Menu_H_DEFINED
