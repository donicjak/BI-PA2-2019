#ifndef Initialize_H_DEFINED
#define Initialize_H_DEFINED

#include <ncurses.h>


namespace Initialize
{

    void init(); // method for initializing and displaying screen

    void exit(); // erasing content and closing down a screen
}

#endif //Initialize_H_DEFINED
