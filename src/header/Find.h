#ifndef Find_H_DEFINED
#define Find_H_DEFINED

#include <iostream>
#include <vector>

using namespace std;

//parent class for searching
class Find
{
    protected:
    string f_expression; // expression based on which methods will be searching for files.

    public:
    Find ( string expression )
    : f_expression ( expression ) {}
    
    virtual ~Find()
    {
    }

    void init(); // initializing screen, will be overwriten in child classes 
    virtual bool check_file( string fn ) = 0; // check, if files fits the criteria, overwriten in child classes 
    void rem_files(); // removes a file from vector
    void all_files (); // saves all files from vector
    void print_files (); // print all files from vector 
    void find_files (); 
    void file_opener (); // method for opening file
    void clean_win();
    string get_String (); // method to store input from user into a string
    bool has_suffix( const string &str, const string &suffix ); // method to check if string has certain suffix (=ending)

    private:
    vector <string> files; // vector of strings representing files

};

// child class representing search of category
class Category : public Find 
{
    public:
    Category ( string Category )
    : Find ( Category ) {}  

    bool check_file( string fn )  override;

};

// child class representing search of tag
class Tag : public Find
{
    public:
    Tag ( string Tag )
    : Find ( Tag ) {}

    bool check_file ( string fn) override;
};

// child class representing search of text
class Text : public Find
{
    public:
    Text ( string Text )
    : Find ( Text ) {}

    bool check_file ( string fn) override;
};

#endif //Find_H_DEFINED