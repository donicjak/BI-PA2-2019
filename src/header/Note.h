#ifndef Note_H_DEFINED
#define Note_H_DEFINED

#include <ncurses.h>
#include <vector>
#include "Memory.h"

class Note
{
    friend class FileSaver; // to znamena, ze filesaver muze pristupovat k jakymkoliv metodam nebo clenum Note.
    
    private:
    int pos_X, pos_Y; // position in a note
    int lb; 
    int format; // helps to detect what format of note is being created
    char current_Mode; // holds a current mode
    Memory * buff; // buffer for holding a text
    string fn; // holds a filename
    string status; // string for printing out a status to a user
    string command; 
    string tag; // 1 tag which will be stored in a vector of tags
    string category;  // 
    vector < string > tags; // vector of string representing all tags
    bool flag; 

    bool execute_Command();
    void m_Up ();
    void m_Left ();
    void m_Down ();
    void m_RIght ();
    string tos( const int & i );
    void del_Line ( int number );
    void del_Line ();
    void save_File ();

    public:
    bool change_status;
    Note( string filename, const int & format );
    char get_currentMode();
    string get_Category();
    void text_open ( string filename );
    void md_open ( string filename );
    void save_text();
    void save_md();
    void printf_Buff();
    void print_StatusLine();
    void update_Status();
    void handle_Input( int c );
    void handle_menu( const int & c );
    void handle_write ( const int & c );
    void handle_category ( const int & c );
    void handle_tags ( const int & c );
    void print_category ( const int & c );
    void print_tags ( const int & c );
};



#endif //Note_H_DEFINED