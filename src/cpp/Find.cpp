#include "../header/Find.h"
#include "../header/Window.h"
#include <ncurses.h>
#include <iostream>
#include <string>
#include <dirent.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

#define CAT "Category:"
#define TAG "Tags:"
#define DIRECTORY "./notes/"

/**
 * Method which finds all files in set directory
  and stores their names in a vector of strings 
 * */
void Find::all_files()
{
    string current = ".";
    string parent = "..";
    DIR *dpdf;
    struct dirent *epdf;
    dpdf = opendir( DIRECTORY ); // opens a directory
    if (dpdf != NULL){ // if directory not empty
    while (((epdf = readdir(dpdf)) != NULL )) // while there are files to read in directory
    {
       if ( epdf->d_name == current || epdf->d_name == parent ) // if file is curent or parent, do nothing
            continue;

       files . push_back ( epdf -> d_name);         // saving files in a vector
   }
    }
}
//Basic method to print out content of vector
void Find::print_files()
{
    for ( auto it = files . begin (); it != files . end(); ++it )
    printw("%s ", (*it) . c_str());
        
}

//Helping method to call other methods
void Find::find_files()
{
    all_files();
    rem_files();
}

// initialization in case of tag

void Find::init()
{
    clear();
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, true);
    find_files();
}

/**
 * Method which takes entire vector of files
 * and in loop calls a check_file method for
 * controling if file fits the criteria.
 * If it doesnt, file is remvoed from vector.
 * */
void Find::rem_files()
{
    printw("\n");
    //printw("We are in rem_files \n");
    //getch();
    for ( auto it = files . begin (); it != files . end(); ++it )
    {
        if ( check_file( (*it ) ) == false ) // calling a method to check wheter file works.
        {
            files . erase ( it );
            it--;
        }
    }
    print_files(); // prints all files fitting the criteria
    file_opener(); // 
}

/**
 * Method which offers user to open
 * one of files fitting the criteria
 * If user types in wrong file name,
 * method asks him to retype his choice.
 * Then it scans the suffix of file 
 * and opens the file
 * */
void Find::file_opener()
{
    printw("\n");
    printw("Choose file you with to open: \n");
    string filename = get_String();

    while ( true )
    {
        if ( find ( files . begin (), files . end (), filename ) == files . end () ) // checks if file with the name user typed is in a vector
        {
            printw("This file doest not exist or doesn't fit the requriement! \n");
            printw("Please try again. \n");
            filename = get_String(); // reads a input from user again
        }
        else
        {
            if ( has_suffix( filename, ".txt" ) == true ) // checking if filename is of format txt
            {
                string result;
                result = filename . substr ( 0, filename.size() - 4 );  // cutting out suffix
                Window::init( result, 1 ); // opening file
                break;
            }
            else
            {
                string result;
                result = filename . substr ( 0, filename.size() - 3 );  // cutting out suffix
                Window::init( result, 2 ); // opening file.
                break;
            }
            
        }
        
    }
}

//basic method which reads input from user and stores it in a string.
string Find::get_String()
{
    string filename;
    nocbreak();
    echo();

    int ch = getch();

    while ( ch != '\n' )
    {
        filename . push_back ( ch );
        ch = getch();
    }

    return filename;
}

void Find::clean_win ( )
{
    erase();
    refresh();
    initscr();
    noecho();
    cbreak(); 
}

//recursive method which check whever string has specific suffix.
bool Find::has_suffix( const string &str, const string &suffix )
{
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

/**
 * Checking if file fits the criteria
 * in case user selected to search
 * for a category. Method goes
 * through all data in a file 
 * and check if category fits
 * the criteria. 
 * */
bool Category::check_file ( string fn )
{
   // printw("Entering check_file ");
    ifstream infile ( ( DIRECTORY + fn ) . c_str() );
    if ( infile . is_open() )
    {
        while ( !infile . eof() )
        {
            string tmp;
            getline ( infile, tmp );
            istringstream iss ( tmp );
            string first_word, word;
            iss >> first_word;

            if ( first_word == CAT ) // if first word on line is Category: meaning following word is a category
            {
                iss >> word;
                if ( word == f_expression ) // if category of file fits the criteria returns true.
                return true;

                return false; // in other situations returns false.
            }
        }
    }
    else
    {
        cerr << "Unable to open a file: '" << fn << "'\n";
    }
return false;     
}

/**
 * Checking if file fits the criteria
 * in case user selected to search
 * for a tag. Method goes
 * through all data in a file 
 * and check if tag fits
 * the criteria. 
 * */

bool Tag::check_file ( string fn )
{
    ifstream infile ( ( DIRECTORY + fn ) . c_str() );
    if ( infile . is_open() )
    {
        while ( !infile . eof() )
        {
            string tmp;
            getline ( infile, tmp );
            istringstream iss ( tmp );
            string first_word, word;
            iss >> first_word;

            if ( first_word == TAG ) // if first word on a line is TAGS:
            {
                while ( iss >> word ) // reads all following words 
                {
                    if ( word == f_expression ) // if the word fits the tag user set to search returns true
                    return true;
                }
            }
        }
    }
    else
    {
        cerr << "Unable to open a file: '" << fn << "'\n";
    }
return false;     
}

/**
 * Checking if file fits the criteria
 * in case user selected to search
 * for a text. Method goes
 * through all data in a file 
 * and check if text fits
 * the criteria. 
 * */

bool Text::check_file ( string fn )
{
    ifstream infile ( ( DIRECTORY + fn ) . c_str() );
    if ( infile . is_open() )
    {
        while ( !infile . eof() )
        {
            string first_word = "";

            while ( first_word != CAT ) // reading lines until first word on line is Category: 
            {
                string tmp;
                getline ( infile, tmp );
                istringstream iss ( tmp );
                iss >> first_word;
                if ( tmp.find(f_expression) != std::string::npos ) // checks if the expression is in the current line
                {
                    return true; // if it is there return true
                }
            }
            return false; // else return false
        }
    }
    else
    {
        cerr << "Unable to open a file: '" << fn << "'\n";
    }
return false;     
}
