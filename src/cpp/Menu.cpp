#include "../header/Menu.h"
#include "../header/Initialize.h"
#include "../header/Window.h" 
#include "../header/Find.h"

#include <ncurses.h>
#include <string>
#include <iostream>
#include <string>
#include <dirent.h>
#define directory "./notes/"
#define txt ".txt"
#define md ".md"

using namespace std;

// recursive function which determinetes wheter string has specifi suffix.
bool Menu::has_suffix(const std::string &str, const std::string &suffix)
{
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

/**
 * Initialization of a new clean screen
 * that displays all notes in 
 * selected format.
 * */
void Menu::clean_win( const int & format )
{
    erase();
    refresh();
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, true);
    list_files( format );
}

//Initialization of a new clean screan.
void Menu::clean_win ( )
{
    erase();
    refresh();
    initscr();
    noecho();
    cbreak(); 
}

// reads a string from user
string Menu::get_String()
{
    string filename;
    nocbreak();
    echo();
    int ch = getch();

    while ( ch != '\n' )
    {
        filename.push_back( ch );
        ch = getch();
    }

    return filename;
}

//Displays all Notes
void Menu::list_files( const int & format )
{
    DIR *dpdf;
    string current = ".";
    string parent = "..";

    struct dirent *epdf;
    dpdf = opendir( directory ); //Opens a directory for notes
    if (dpdf != NULL){
    while (((epdf = readdir(dpdf)) != NULL )) // while there are files to read from
    {
       if ( epdf->d_name == current || epdf->d_name == parent ) // skips current and parent dir
            continue;
            
       else if ( format == 1 ) // if format is set to 1 skips all .md files
        {
            if ( has_suffix( epdf->d_name, md ) )
            continue;
        }
        else if ( format == 2 ) // if format is set to 2 skips all .txt files
        {
            if ( has_suffix( epdf->d_name, txt ) )
            continue;
        }
        printw("%s ",epdf->d_name); // displaying files
             
   }
   
}
closedir(dpdf); // closing a directory we were reading from.

}

//Intitialazing main menu
void Menu::init_main()
{
    WINDOW * win = newwin ( 6, 14, 1, 1 ); //Initializing a window in a screen
    box( win, 0, 0 ); // sets default borders for menu
    refresh();
    wrefresh( win ); //updates the terminal screen
    keypad ( win, true );
    string choices[ 3 ] = { "Create/Open", "Search", "Exit" };
    int choice;
    int highlight = 0;

    // iterates through menu and highlights the choice we are currently on
    while ( true )
    {
        for ( int i = 0; i < 3; i++ )
        {
            if ( i == highlight )
            wattron ( win, A_REVERSE );
            mvwprintw ( win, i + 1, 1, choices[i].c_str() );
            wattroff( win, A_REVERSE );
        } 
        choice = wgetch ( win );

        switch ( choice )
        {
            case KEY_UP:
            highlight--;

            if(highlight == -1) //protects from getting out of the menu
            highlight = 0;
            break;

            case KEY_DOWN:
            highlight++;

            if(highlight == 3) //protects from getting out of the menu
            highlight = 2;
            break;

            default:
            break;
        }
        if ( choice == 10 ) // on pressed enter
        break;
    }
   
    select_main ( highlight );        
}

//Method for calling other methods in cases of our choice
void Menu::select_main ( int highlight )
{
   switch ( highlight )
   {
       case 0 : 
       init_create();
       break;

       case 1:
       init_search();
       break;

       case 2:
       erase();
       endwin();
       break;

   }
}

//Intitialazing create/open menu
void Menu::init_create()
{
    erase();
    WINDOW * win = newwin ( 5, 14, 1, 1 ); //Initializing a window in a screen
    box( win, 0, 0 ); // sets default borders for menu
    refresh();
    wrefresh( win );//updates the terminal screen
    keypad ( win, true );
    string choices[ 3 ] = { "PlainText", "MarkDown", "Return"};
    int choice;
    int highlight = 0;

// iterates through menu and highlights the choice we are currently on
    while ( true )
    {
        for ( int i = 0; i < 3; i++ )
        {
            if ( i == highlight )
            wattron ( win, A_REVERSE );
            mvwprintw ( win, i + 1, 1, choices[i].c_str() );
            wattroff( win, A_REVERSE );
        } 
        choice = wgetch ( win );

        switch ( choice )
        {
            case KEY_UP:
            highlight--;

            if(highlight == -1) //protects from getting out of the menu
            highlight = 0;
            break;

            case KEY_DOWN:
            highlight++;

            if(highlight == 3) //protects from getting out of the menu
            highlight = 2;
            break;

            default:
            break;
        }
        if ( choice == 10 ) // on pressed enter
        break;
    }
    select_create( highlight ); 
}


//Method for calling other methods in cases of our choice
void Menu::select_create ( int highlight )
{
    switch ( highlight )
    {
        case 0:
        clean_win ( 1 );
        printw(" \n \n \n");
        printw("Write a name of a note to create/open.\n");
        Window::init( get_String(), 1 );
        break;

        case 1:
        clean_win ( 2 );
        printw("\n \n \n");
        printw("Write a name of a note to create/open.\n");
        Window::init ( get_String(), 2 );
        break;

        case 2:
        erase();
        init_main();
        break; 
    }    
}

void Menu::init_search()
{
    erase();
    WINDOW * win = newwin ( 6, 14, 1, 1 ); //Initializing a window in a screen
    box( win, 0, 0 ); // sets default borders for menu
    refresh();
    wrefresh( win ); //updates the terminal screen
    keypad ( win, true );

    string choices[ 4 ] = { "Category", "Tag", "Text","Return"};
    int choice;
    int highlight = 0;

// iterates through menu and highlights the choice we are currently on
    while ( true )
    {
        for ( int i = 0; i < 4; i++ )
        {
            if ( i == highlight )
            wattron ( win, A_REVERSE );
            mvwprintw ( win, i + 1, 1, choices[i].c_str() );
            wattroff( win, A_REVERSE );
        } 
        choice = wgetch ( win );

        switch ( choice )
        {
            case KEY_UP:
            highlight--;

            if(highlight == -1) //protects from getting out of the menu
            highlight = 0;
            break;

            case KEY_DOWN:
            highlight++;    

            if(highlight == 4) //protects from getting out of the menu
            highlight = 3;
            break;

            default:
            break;
        }
        if ( choice == 10 ) // on enter pressed breaks
        break;
    }
    select_search( highlight );
}


//Method for calling other methods in cases of our choice
void Menu::select_search ( int highlight ) 
{
    /**
     * Object of an abstract class.
     * We set correct implementation based
     * on a choice of user.
     * At the end of switch
     * we call init, which calls find files, where
     * specific implementation is used based on user
     * choice, which represents polymorfism.
     * */
    Find * selectedFind = nullptr; 
    switch ( highlight )
    {
        case 0:
        {
        clean_win();
        printw("Write a category to search: \n");
        selectedFind = new Category( get_String() );
        }
        break;

        case 1:
        {
        clean_win();
        printw("Write a tag to search: \n");
        selectedFind = new Tag( get_String() );
        }
        break;

        case 2:
        {
        clean_win();
        printw("Write a text to search: \n");
        selectedFind = new Text( get_String() );
        }
        break;

        case 3:
        {   
        erase();
        init_main();    
        }
        break;
    }
    
    if (selectedFind != nullptr)
    {
        selectedFind->init();
        delete selectedFind;
    }
}