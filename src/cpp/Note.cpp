#include "../header/Note.h"
#include "../header/Menu.h"
#include "../header/FileSaver.h"

#include <ncurses.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iterator>
#include <cstring>

#define CAT "Category:"
#define TAG "Tags:"
#define TXT_FORMAT 1
#define DIRECTORY "./notes/"
#define TXT ".txt"
#define MD ".md"

Note::Note ( string filename, const int & nr )
{
   pos_X = pos_Y = 0;
   current_Mode = 'm';
   command = "";
   status = "Menu mode";
   category = "";
   tag = "";
   fn = filename;
   lb = 0;
   flag = false;
   change_status = true;
   buff = new Memory ();
   format = nr;
   
   if ( format == TXT_FORMAT ) // if number is 1 creates/opens note in .txt format
   text_open ( filename );
    
   else // else creates//opens note in .md format
   md_open ( filename ); 
}

void Note::text_open ( string filename )
{
    ifstream infile (( DIRECTORY + filename + TXT ) .c_str());  // creates//opens note under filename adding .txt suffix
    if ( infile . is_open () ) // if sucessfully creates//opens
    {
        while ( !infile . eof() ) // reading from file until EOF
        {
            string tmp;
            getline ( infile, tmp ); // gets line of text in file
            istringstream iss(tmp);
            string first_word, word;
            iss >> first_word; // gets first word from a line
            if ( first_word == CAT )
            {
                iss >> word;
                category = word;  // if first word on a line was category then sets category to a second word on a line
            }
            else if ( first_word == TAG )
            {
                while ( iss >> word )
                {
                    tags . push_back ( word ); // if first word on a line was tags then pushes all next words into a vector
                }
            }
            else
            {
                buff -> app_Line ( tmp ); // in all other cases append line in buffer
            }

        }
    }
    else
    {
        cerr << "Unable to open a file: '" << filename << "'\n"; // if cannot create/open file prints an error
        buff -> app_Line( "" );
    }
    infile.close();   //closes file
}

void Note::md_open ( string filename )
{
    ifstream infile (( DIRECTORY+ filename + MD ) .c_str());  // creates//opens note under filename adding .txt suffix
    if ( infile . is_open () ) // if sucessfully creates//opens
    {
        while ( !infile . eof() ) // reading from file until EOF
        {
            string tmp;
            getline ( infile, tmp ); // gets line of text in file
            istringstream iss(tmp);
            string first_word, word;
            iss >> first_word; // gets first word from a line
            if ( first_word == CAT )
            {
                iss >> word;
                category = word;  // if first word on a line was category then sets category to a second word on a line
            }
            else if ( first_word == TAG )
            {
                while ( iss >> word )
                {
                    tags . push_back ( word ); // if first word on a line was tags then pushes all next words into a vector
                }
            }
            else
            {
                buff -> app_Line ( tmp ); // in all other cases append line in buffer
            }

        }
    }
    else
    {
        cerr << "Unable to open a file: '" << filename << "'\n"; // if cannot create/open file prints an error
        buff -> app_Line( "" );
    }
    infile.close();   //closes file
}


/**
 * Based of current_mode sets a status
 * which is then displayed to a user.
 * */

void Note::update_Status ()
{
    switch ( current_Mode )
    {
        case 'm':
        if ( command . empty() )
            status = "Menu: ";
        else
        {
            status = command;
        }
        break;

        case 'c':
        status = category;
        break;

        case 't':
        status = tag;
        break;

        case 'g':
        status = "Category: " + category;
        break;    

        case 'j':
        for ( auto it = tags . begin(); it != tags . end(); ++it )
        status += " " + (*it);
        break;

        case 'w':
        status = "Write mode";
        break;

        case 'e':
        status = "Exit";
        break;
    }
    status += "\tCOL: " + tos( pos_X ) + "\tROW: " + tos( lb );
}

string Note::tos( const int & i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}

/**
 * This method handles all input
 * depending on a current mode
 * calls a method to handle specific
 * situation.
 * */
void Note::handle_Input ( int c )
{
   change_status = true;
   switch ( current_Mode )
   {
       case 'm':
       handle_menu( c );
       break;

       case 'w':
       handle_write( c );
       break;

       case 'c':
       handle_category( c );
       break;

       case 't':
       handle_tags ( c );
       break;

       case 'g':
       print_category( c );
       break;

       case 'j':
       print_tags ( c );
       break;

       case 'e':
       erase();
       refresh();
       endwin();
       break;

       default:;
   }
   
}

//handles all input when in menu mode
void Note::handle_menu ( const int &  c )
{
   switch ( c )
        {
        case KEY_LEFT:
            m_Left();
            break;
        case KEY_RIGHT:
            m_RIght();
            break;
        case KEY_UP:
            m_Up();
            break;
        case KEY_DOWN:
            m_Down();
            break;
        case 27: // represent ESC key
            command.clear();
            break;
        case KEY_ENTER:
        case 10:  // entering command
            execute_Command();
            break;
        case 127:
        case KEY_BACKSPACE:
        case KEY_DC: // deleteing command
            if( !command.empty())
                {command.erase(command.length()-1, 1);}
                break;
        default:
            command += string(1, char(c));  // by default adds characters to command one by one
            break;
        }  
}


// handle in case of insert mode
void Note::handle_write ( const int & c )
{
 switch( c )
        {
            case 127:
            case KEY_BACKSPACE:
            if ( pos_X == 0 && pos_Y > 0 ) // if input is in the left but there are more lines
            {
                pos_X = buff -> text_lines [ pos_Y - 1 ] . length(); // removes the last char from lien
                buff -> text_lines [ pos_Y - 1 ] += buff -> text_lines [ pos_Y ]; // sets current line to line before
                del_Line();
                m_Up();
            }
            else if ( pos_X > 0 ) // if input is not in upper left corner
            {
                buff -> text_lines [ pos_Y ].erase( --pos_X, 1 );
            }
            break;

            case 27: // when users presses escape, sets mode back to menu
            current_Mode = 'm';
            break;
            
            case KEY_ENTER:
            case 10: // entering in a line
            if( pos_X < (int)buff->text_lines[ pos_Y + lb ].length() - 1 ) /// if users enter in the middle of line, puts the splitted next on new line
            {
                buff-> in_Line( buff-> text_lines [ pos_Y + lb ]. substr( pos_X, buff-> text_lines [ pos_Y + lb ]. length() - pos_X), pos_Y + 1);
                buff->text_lines[pos_Y + lb ].erase(pos_X, buff->text_lines[pos_Y + lb ].length() - pos_X);
            }
            else
            {
                buff->in_Line("", pos_Y+1 + lb ); // makes new line empty
            }
            m_Down();
            break;
            case KEY_LEFT: // moving left on a editor
                m_Left();
                break;
            case KEY_RIGHT: // moving right on a editor
                m_RIght();
                break;
            case KEY_UP: // moving up on a editor
                m_Up();
                break;
            case KEY_DOWN: // moving down on a editor
                m_Down();
                break;
            case KEY_BTAB:
            case KEY_CTAB:
            case KEY_STAB:
            case KEY_CATAB:
            case 9: // all cases for a tab in a text
            buff->text_lines[ pos_Y + lb ].insert( pos_X, 4, ' ');
            pos_X += 4;
            break;

            default: // by default insert a line into a vector of string in buffer
            buff -> text_lines[ pos_Y + lb ] . insert ( pos_X, 1, char (c) );
            pos_X++;
            break;    

        }
}

//handles category
void Note::handle_category ( const int & c )
{
        switch ( c )
        {
            case KEY_ENTER:
            case 10: // on enter saves category and gets back to menu mode
            current_Mode = 'm';
            break;

            case KEY_END: // when user presse END deletes category
            category = "";
            break;
            
            case 127:
            case KEY_BACKSPACE:
            case KEY_DC: // deleting command
            if( !command.empty())
                {command.erase(command.length()-1, 1);}
                break;

            default: 
            category += string (1, char(c)); // adding to category one character at a time
            break;
        }
}

// handles in case of adding tags
void Note::handle_tags ( const int & c )
{
       switch ( c )
        {
            case KEY_ENTER: // when users presses enter adds tag into a vector of tags
            case 10:
            tags . push_back ( tag );
            tag = "";
            break;

            case KEY_END: // on end button clears all tags in vector
            tags.clear();
            break;

            case 127:
            case KEY_BACKSPACE:
            case KEY_DC: // deleteing command
            if( !command.empty())
                {command.erase(command.length()-1, 1);}
                break;

            case 27: // when pressed ESC button sets mode back to menu
            current_Mode = 'm';
            break;

            default:
            tag += string (1, char(c)); // adds character to a tag which will be inserted into a vector
            break;
        }
}

//method returning a category to a user
void Note::print_category ( const int & c )
{
    switch ( c )
    {
        case 10: // when pressed enter return back to menu
        current_Mode = 'm';
        break;
    }
}

//method returning all tags to a user
void Note::print_tags ( const int & c )
{
    switch ( c )
    {
        case 10: // when pressed enter return back to menu
        current_Mode = 'm';
        break;
    }
}

//method for moving right in editor
void Note::m_RIght ()
{
    if ( pos_X + 1 <= (int)buff -> text_lines [ pos_Y ] . length() && pos_X + 1 < COLS ) //if positing on the line is not in right border
    {
        pos_X++;
        move( pos_Y, pos_X );
    }
}

//method for moving up in editor
void Note::m_Up()
{
    if( pos_Y - 1 >= 0 ) // if position on the vertical map is not in the top
        pos_Y--;
    else if ( pos_Y - 1 < 0 && lb > 0 )
    {
        lb--;
    }
    if( pos_X >= (int)buff-> text_lines [ pos_Y ].length()) // if there is less chars in the current line then current position
        pos_X = buff-> text_lines [ pos_Y ].length();
    move( pos_Y, pos_X );
}

//method for moving down in editor
void Note::m_Down()
{
    if( pos_Y + 1 < LINES - 1 && pos_Y + 1 < (int)buff-> text_lines.size()) // if position on the vertical map is not in the bottom
        pos_Y++;
    else if ( lb + pos_Y < (int)buff -> text_lines . size () )
    {
        flag = true;
        lb++;
    }
    if( pos_X >= (int)buff-> text_lines [ pos_Y ].length()) // if there is less characters in current line then current position is.
        pos_X = buff-> text_lines[pos_Y].length();
    move( pos_Y, pos_X );
}

//method for moving left in editor
void Note::m_Left()
{
    if( pos_X - 1 >= 0) // if position on the horizontal scale is not completely in the left
    {
        pos_X--;
        move( pos_Y, pos_X );
    }
}

//return a mode of current Note represented by single character
char Note::get_currentMode()
{
    return current_Mode;
}

//return a category of a current Note
string Note::get_Category()
{
    return category;
}
/**
 * Method for printing out a buffer
 * Print all lines in for cycle.
 * */

void Note::printf_Buff()
{
    int pom = 0;
    for( int i = lb; pom<LINES - 1; i++ )
    {
        if( i >= (int)buff -> text_lines . size())
        {
        }
        else
        {
            mvprintw( pom, 0, buff -> text_lines [ i ] . c_str());
        }
        clrtoeol();
        pom++;
        
    }
    move( pos_Y, pos_X );
}

//method for printing out a status line
void Note::print_StatusLine()
{
    if( flag )
        attron(A_BOLD); //atrron is a routine which turns on the named attributes without effectinc others
    attron(A_REVERSE);
    mvprintw(LINES-1, 0, status.c_str());
    clrtoeol();
    if( flag )
        attroff(A_BOLD);
    attroff(A_REVERSE);
}

//deletes a line
void Note::del_Line()
{
    buff -> rem_Line( pos_Y );
}
//deletes a line on a specific position
void Note::del_Line ( int nr )
{
    buff -> rem_Line ( nr );
}

//method to save a file
void Note::save_File()
{   
    
    if ( format == 1 ) // if a format variable is set to value 1 saves note in a txt format
    save_text();
    else
    save_md();
}

//saves the file in a .txt format
void Note::save_text ( )
{
    FileSaver s(DIRECTORY, TXT);

    s.saveNoteToFile(*this, fn);
}

//saves the file in a .md format
void Note::save_md ( )
{
    FileSaver s(DIRECTORY, MD);

    s.saveNoteToFile(*this, fn);
}
/**
 * Method which executes command set
 * in menu mode.
 * Dependign on a choice of user sets
 * current mode accordingly.
 * */

bool Note::execute_Command()
{
    if ( command == "e" )
        current_Mode = 'e';
    else if ( command == "w" )
        current_Mode = 'w';
    else if ( command == "c")
        current_Mode = 'c';
    else if ( command == "t" )
        current_Mode = 't';
    else if ( command == "j" )
        current_Mode = 'j';
    else if ( command == "g" )
        current_Mode = 'g';
    else if ( command == "s" )
    {
        change_status = false;
        save_File();
    }

    command = "";
    return true;
}