#include "../header/Window.h"
#include "../header/Note.h" 
#include <ncurses.h>
#include <iostream>
#include <string>

using namespace std;

//Initializing new screen

void Window::init( string filename, const int & format )
{
    clear();
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, true);
    start_editor ( filename, format );
}
/**
 * Method to initialize an instance
 * of note, creating or opening it
 * under passed filename.
 * Nr defines format of note
 * */

void Window::start_editor ( string filename, const int & format )
{
        Note x = Note( filename, format );

        while ( x . get_currentMode( ) != 'e' ) // Handling input in Note until mode is set to 'e' (=exit)
        {
        if( x. change_status) 
            x.update_Status();
        
        x.print_StatusLine(); // Printing out a status line.
        x.printf_Buff(); // Printing out a buffer, by lines.
        int c = getch();   // Reading input from user .        
        x.handle_Input(c); // Handeling the input.
        }

        end_scr();
}

void Window::end_scr()
{
    refresh();
    endwin();
}

