#include "../header/NoteData.h"

NoteData::NoteData()
{
}

NoteData::~NoteData()
{
}

void NoteData::removeLine(int lineNumber)
{
    text_lines_ . erase(text_lines_ . begin() + lineNumber); //removes line on specific position
}

void NoteData::apendLine(std::string line)
{
    line = removeTabs(line);
    text_lines_ . push_back(line); // appends line to a existing one
}

void NoteData::insertLine(std::string line, int lineNumber)
{
    line = removeTabs(line);
    text_lines_ . insert(text_lines_ . begin() + lineNumber, line); // inserts line to a specific position
}
/**
 * This function recursively finds a tab in a line.
 * When a tab is found it goes again until no more
 * tabs could be found, then returns the line.
 * */
std::string NoteData::removeTabs(std::string line)
{
    int tab = line . find("\t");
    if (tab == (int) line . npos)
        return line;

    return removeTabs(line . replace(tab, 1, "    "));  
}
