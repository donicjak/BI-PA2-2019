#include "../header/Menu.h"
#include "../header/Initialize.h"

int main ( int argc, char const * argv[] )
{
    Initialize::init(); //Initializing starting screen

    Menu::init_main(); // Calling in a menu window

    Initialize::exit(); //Closing screen

    return 0;
}