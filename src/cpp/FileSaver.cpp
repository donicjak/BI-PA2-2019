#include "../header/FileSaver.h"
#include "../header/Note.h"

using namespace std;

//intializing constructor
FileSaver::FileSaver(std::string baseDirectoryPath, std::string fileNameExtension)
: baseDirectoryPath_(baseDirectoryPath), fileNameExtension_(fileNameExtension)
{
}

//basic destructor.
FileSaver::~FileSaver()
{
}

/**
 * Method which saves the note
 * First it save the notes by lines,
 * then it saves the category and lastly
 * all tags.
 * */
void FileSaver::saveNoteToFile(Note& noteToSave, std::string& fileName)
{
    ofstream f((baseDirectoryPath_ + fileName + fileNameExtension_) . c_str()); //Output stream class to operate on files in correct filename
    
    vector<string> text_lines = noteToSave.buff->text_lines;
    
    if (f . is_open()) // if file sucessfully opens
    {
        for (unsigned i = 0; i < text_lines . size(); i++) {
            f << text_lines [ i ] << endl; // first saving all written text in a loop
        }

        /**
         * On last 2 lines saves category
         * and tags. In both cases prints a string
         * to search before actual varaibles.
         * */
        f << "Category: ";
        f << noteToSave.category + "\n";
        
        vector<string> tags = noteToSave.tags;
        f << "Tags:";
        for (auto it = tags . begin(); it != tags . end(); ++it)
            f << " " + (*it);
        f << "\n";
        noteToSave.status = "Saved!"; // sets a status variable for user to check if saved went through
    }
    else {
        noteToSave.status = "Error!"; // if file cannot be accesed prints an error warning
    }
    
    f . close(); // closes the file
}



