#include "../header/Memory.h"

Memory::Memory() {}

/**
 * This function recursively finds a tab in a line.
 * When a tab is found it goes again until no more
 * tabs could be found, then returns the line.
 * */
string Memory::rem_Tabs( string line )
{   
    int tab = line . find ("\t");
    if ( tab == (int)line . npos )
        return line;

    return rem_Tabs( line . replace ( tab, 1, "    "));   
}

void Memory::rem_Line ( const int & nr )
{
    text_lines . erase( text_lines . begin() + nr); //removes line on specific position
}

void Memory::app_Line( string line )
{
    line = rem_Tabs( line ); 
    text_lines . push_back ( line ); // appends line to a existing one
}

void Memory::in_Line ( string line, const int & nr )
{
    line = rem_Tabs ( line );
    text_lines . insert ( text_lines . begin () + nr, line ); // inserts line to a specific position
}