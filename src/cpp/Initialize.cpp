#include "../header/Initialize.h"

#include <ncurses.h>
#include <string>

using namespace std;

/**
 * Starting method initializing ncurses screen 
 * */

void Initialize::init()
{
    initscr(); // must be called first
    noecho(); //  disable buffering of typed characters
    cbreak(); // supress automatic echoing of typed characters
    keypad(stdscr, TRUE); // need this to capture special keystrokes such as delete and backspace

}
void Initialize::exit()
{
    erase(); // erase everything written in window
    refresh(); 
    endwin(); //return to reminal settings
}
