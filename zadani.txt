Poznámkový blok

Program pro správu poznámek, který má následující funkcionalitu:

Reprezentace poznámek v plaintextu a markdownu.
Uživatelské rozhraní, které zobrazuje poznámky.
Systém štítků a tagů.
Vyhledávání poznámek na základě kategorií, tagů či textu obsaženého v poznámce.
Exportovaní a improtovaní poznámek, i na základě vyhledávání.

Využítí polymorfismu:
Více druhů výhledávání.
