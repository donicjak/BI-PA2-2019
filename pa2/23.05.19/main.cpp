/**
 * Zadani:
 * Cilem je implementovat trasu metra. Vlak vyjizdi vzdy z jedne konecne, dojede na druhou, otoci se a jede zpet.
 * Za kazdou prestupni stanici plati cestujici 1 platidlo. 
 * 
 * void add ( istringstream & is )
 * Prida novou trasu metra. Vlak jezdi po jedne trase tam a zpet.
 * 
 * set<string> & Dest (const string & from, int maxCost)
 * Vraci vsechny stanice, kam je mozne dojet z nastupni stanice (from) za urcitou cenu (maxCost).
 * 
 * tohle je zadani z predeslych let, v tom aktualnim prohledavame dle poctu prestupu, ne podle poctu projetych stanic.
 * 
 * Myslenka na reseni:
 * Linky reprezentuji jednotlive vrcholy a pokud sdileji nejake stanice, tak to je reprezentovano hranou mezi nimi.
 * Pak na to pustime BFS a pruchodem se zjisti, ktere vsechny linky se daji na dany pocet prestupu navstivit. Staci uz pridat
 * jmena stanic z linek do vystupniho setu.
 * Jediné důležité je si uvědomit, že výchozí stanice může být i přestupní,
 * proto je nutné do BFS už nazačátku vložit všechna čísla linek (vrcholů), do kterých spadá daná stanice.
 * */



#include <sstream>
#include <string>
#include <algorithm>
#include <map>
#include <set>
#include <list>
#include <deque>
#include <queue>
#include <stack>
#include <cassert>

using namespace std;

class CMHD
{
    public:
    //metoda, jenz prida novou trasu metra, vlak jezdi po jedne trase tam a zpet.
    //Posilame do nej string nazvu stanic oddelenych pomoci /n
        void Add ( istringstream & is )
        {
            string str; //pomocna promenna
            size_t num_of_lines = m_Names_On_Line.size(); // do teto promenne se ulozi velikost Names_on_line, tedy pocet stanic linky
            while ( getline(is, str) ) // nacitame pon /n
            {
                // add edges from other lines to this line
                if ( m_Name_to_Num.count(str) != 0 ) // prohleda mapu na elementy s ekv. klicem a vrati 1 pokud tam je, 0 pokud ne.
                    for ( size_t num : m_Name_to_Num[str] ) // pro vsechny linky dane stanice
                    {
                        m_graph[num_of_lines].insert(num); // vlozim do BFS na zaklade poctu stanic, vlozim danou stanici
                        m_graph[num].insert(num_of_lines); // vlozim do BFS na zaklade dane stanice, vlozim pocet stanic
                    }

                m_Names_On_Line[num_of_lines].insert(str); // dame danou stanici do linky (pocet_stanic, nazev )
                m_Name_to_Num[str].insert(num_of_lines); // dame danou stanici pro check na pocet linek ( nazev, pocet_stanic )
            }
            m_graph[num_of_lines]; // posledni mapa pro BFS. Vlozime pocet linek
        }
        //===============================================================================

        // Vraci vsechny stanice kam je mozne od pocatecni dojet, maxCost urcuje pocet prestupu -> 1 prestup 1 value
        set < string > Dest ( const string & from, int maxCost ) const
        {
            queue < size_t > myQ; // container first in, first out.
            set < string > retSet = { from };  // set nastavime na zacatku na pocatecni bod
            map < size_t, int > visitedAt; // mapa, klic je size_t, value je int

            if ( m_Name_to_Num.count(from) == 0 ) // pokud ma stanice jen jednu linku, returnuje
                return retSet;

            for ( const auto & line : m_Name_to_Num.find(from)->second )  // for kazdou linku, jenz odpovida klici from stanice
            {
                visitedAt[line] = 0; // nastavime ze jsem navstivili
                myQ.push(line); // pushneme do queue na konec
            }

            while ( ! myQ.empty() ) // dokud neni queue prazdna
            {
                size_t cur = myQ.front(); // nastavime cur na dalsi element v queue
                myQ.pop(); // odstrani dalsi element z queue, odstranuje posledni.
                retSet.insert ( m_Names_On_Line.find(cur)->second.begin(), m_Names_On_Line.find(cur)->second.end() );
                //insertuje do setu na zaklade klice dalsi

                for ( size_t neigh : m_graph.find(cur)->second ) // for klic v BFS
                    if ( visitedAt.count(neigh) == 0 && visitedAt[cur] < maxCost ) // pokud jsem nenavstivili a je mensi nez cena
                    {
                        visitedAt[neigh] = visitedAt[cur] + 1; // visited at nastavime o jednu vis, tedy ze byla navstivena.
                        myQ.push(neigh); // pushneme do que
                    }
            }
            return retSet; // returnujeme 
        }
        //===============================================================================
    private:
        map < size_t, set < string > > m_Names_On_Line; // linka ma stanice, value set stringu, klic je size_t
        map < string, set < size_t > > m_Name_to_Num;   // stanice muze mit vice linek, klic je string, value je set size_t
        map < size_t, set < size_t > > m_graph;         // prestupy mezi linkami, klic je size_t, value je set size_t
        //size_t -> unsigned integer jenz je vysledkem sizeof operatoru
        // mapa -> Mame hodnoty, jenz radime na zaklade klice.
        // set je conteiner, jenz uchovava klice
        // tedy drzime zde mapu klicu a setu_klicu
};


int main ( void )
{
    CMHD city;
    istringstream iss;
    iss.clear();

    iss . str ( "A\nB\nC\nD\nE\n" );
    city . Add ( iss );
    iss . clear();

    iss . str ( "B\nC\nF\nH\n" );
    city . Add ( iss );
    iss . clear();

    iss . str ( "F\nG\nI\nJ\nK\nN\n" );
    city . Add ( iss );
    iss . clear();

    iss . str ( "H\nL\n" );
    city . Add ( iss );
    iss . clear();

    iss . str ( "L\nM\nN\nO\n" );
    city . Add ( iss );
    iss . clear();

    iss . str ( "P\nQ\nR\nN\nS" );
    city . Add ( iss );
    iss . clear();

    assert ( city . Dest ( "S", 0 ) == set < string > ( {"S", "N", "R", "Q", "P"} ) );

    assert ( city . Dest ( "S", 1 ) == set < string > ( { "S", "N", "R", "Q", "P",
                                                        "O", "M", "L",
                                                        "K", "J", "I", "G", "F" } ) );

    assert ( city . Dest ( "N", 0 ) == set < string > ( { "S", "N", "R", "Q", "P",
                                                        "O", "M", "L",
                                                        "K", "J", "I", "G", "F" } ) );

    assert ( city . Dest ( "N", 1 ) == set < string > ( { "S", "N", "R", "Q", "P",
                                                        "O", "M", "L",
                                                        "K", "J", "I", "G", "F",
                                                        "H", "F", "C", "B" } ) );

    assert ( city . Dest ( "N", 2 ) == set < string > ( { "S", "N", "R", "Q", "P",
                                                        "O", "M", "L",
                                                        "K", "J", "I", "G", "F",
                                                        "H", "F", "C", "B",
                                                        "A", "D", "E" } ) );

    assert ( city . Dest ( "unknown", 0 ) == set < string > ( { "unknown" } ) );
    assert ( city . Dest ( "unknown", 1 ) == set < string > ( { "unknown" } ) );
    assert ( city . Dest ( "unknown", 2 ) == set < string > ( { "unknown" }) );

    return 0;
}