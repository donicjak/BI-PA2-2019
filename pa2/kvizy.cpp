Kvizove otazky:

1. linkovani:

//first.cpp
#include <iostream>
#include "header.h"
int main( void ) 
{
    mystery( g );
    std::cout << g_value;
    return 0;
}

//second.cpp
#include "header.h"

void mystery ( int x )
{   
    g_value += x;
}


//header.h

extern int g_value;
void mystery ( int x );



g++ -Wall -pedantic -c first.cpp -o first.o
g++ -Wall -pedantic -c second.cpp -o second.o
g++ -Wall -pedantic first.o second.o -o mystery
./mystery

-> Kompilace nebo linkovani selze.


2. STL

Chceme realizovat tridu pro telefoni seznam zamestnancu firmy. Pro jednoduchost predpokladame, ze telefon lze reprezentovat jak ocele cislo.
a jmeno jako retezec. Dale predpokladame, ze jmena jsou jednoznacna ( kazdy zamestnanec je indetifikovan pouze svym jmenem ). Jedne zamestnanec
 muze mit prideleno az 5 tel. cisel ( napr. pokud pracuje na vice mistech  firmy ) a jedno cislo muze byt sdileno az 5 zamestnanci ( napr. jeden telefon v kancelari ).
 Jake lze pouzit dat. struktury pro realizaci takoveho tle. seznamu, pokud v seznamu chceme vyhledavat dle jmena zamestnance, vkladat i mazat
 rychelji nez linearne?

 map<string,list<int»
 set<pair<string,int»
 map<string,vector<int»
unordered_multimap<string,int>

3. -> 1. kviz

-> Pokud zavolame referenci metodu, ale vracime cistou promenou (napr. int) a ne referenci -> nepujde zkompilovat
-> Pokud neco v ramci metody naalokujeme, ale neuvolnime -> pujde zkompilovat, nespadne, ale neuvolni po sobe pamet.
-> Zavolame metodu, mame je 2, ale lissi se typ parametru ( char, double ), ( int, int ), zavolame char, ale druhy double neprojde a pak si neumi vybrat -> nejde zkompilovat.
-> funkce ktere se lisi pouze v returnu -> nejde zkompilovat.
-> naalokujeme jeden int, ale pak s nim pracujeme jako s polem -> zkopiluje, ale pracujes spatne s pameti neb ospadne.
-> v cyklu musime mazat pole, nikoliv promenou.
-> pokud predavame hodnotu do metody a chceme ji zde upravovat, musime mit referenci, jiank to nemeni hodnotu.
-> pokud predame neco do metody, ale metoda to chce poslat dale, ale nema parametr const -> nejde zkompilovat.
-> funkce ceka napr int, ale my posleme const int -> nejde zkompilovat.
-> vytvorim-li pole, ale neincialuziji jeho hodnoty -> jde zkompilovat, ale vysl. neni def.
-> naalokuji single int, ale pak mazu jako pole -> jde zkompilovat, ale pracuje spatne s pameti.



4. -> 2. kviz

7-----------------------------------------------------------------------------------------------------
class CNUM
{
    public:
    CNUM ( int x ) { m_X = x; }
    CNUM & operator + const ( const CNUM & a ) const { CNUM res ( m_X + a . m_X ); return res; }
    void print ( void ) const ( cout << m_X; )
    private:
    int m_X;
};

int main ( void )
{
    CNUM a ( 41 );
    CNUM c = a + 44;
    c . print();
    return 0;
}

tady je to & na zacatku operatoru CNUM -> nespadne, ale vysledek je nedef. / zkompiluje ale muze spadnout
pretizeny operator CNUM je friend -> nejde zkompilovat
CNUM operator + ( CNUM & a, CNUM & b ) { return a + b; }, pak zavolame CNUM c = a + b -> nejde zkompilovat
CNUM operator + ( const CNUM & a ) const ( return a + m_X; ) a volame CNUM c = a + 57 -> zkompiluje, ale muze spadnout
CNUM operator + ( const CNUM & a ) const { CNUM res ( m_X + a . m_X ); return CNUM ( res ); } -> funguje spravne ale pretezuje nezvyklym zpusobem.


-----------------------------------------------------------------------------------------------------



Tady bude ten velky s poli.

-------------------------------------------------------------------------

class A
{
    public:
        A ( int x ) { m_X = x; cout << x; }
    private:
    int m_X;
};

class B
{
    public:
        B ( int x ) { m_A = A  ( x ); }
    private:
    A m_A;
};

B test ( 61 )

A neni inicializovane ( tedy A ( int x = neco )) -> nejde zkompilovat
je-li inicializovane, da se to v testu z nej, napr. ( A ( int x = 96 ) a volame B test ( 0 ) vypise 960)
B ( int x ) : m_A ( new A (x ) ) { } -> vypise to co je v testu, tedy napr. B test ( 0 ) -> vypise 0
neni inicializovane A, ale mame B ( int x ) : m_A ( x ) { } -> vypise to co je v testu
A je inicializovane, ale mame A * m_a   -> vypise to co je v testu.


-------------------------------------------------------------------------

class CInt
{
    public:
    CInt ( int x ) : m_X ( new int ( x ) ) { }
    CINT ( const CInt & src )    { m_X = src . m_X }
    ~CINT ( void ) { delete m_X; }
    void operator = (int x ) { *m_X = x;}
    void print ( void ) const { cout << *m_X; }
    
    private:
    int * m_X;
}

int main ( void )
{
    CINT a ( 82 ), b ( 4 ), c = a;
    a = 25;
    c . print();
    return 0;
}

-> vychozi -> jde zkompilovat, ale muze spadnout/spadne vytvarime melkou kopii, a mazeme 2x
-> CInt ( const CInt & src ) { m_X = new int ( *src. m_X ); } -> vypiseme 82
-> vychozi bez destruktoru -> printujeme 25 // dokonce i bez druheho konstruktoru

-------------------------------------------------------------------------

class CNUM
{
    public:
    CNUM ( int x ) { m_X = x; }
    CNUM ( const CNUM & src ) { m_X = src . m_X; cout << m_X; }
    friend ostream & operator << ( ostream & os, const CNUM & x );

    private:
    int m_X;
}

ostream & operator << ( ostream & os, const CNUM & x )
{
    os << x . m_X
    return os;
}

CNUM a ( 50 ), b = a;
cout << a;

-> vzor vypise 2x -> 5050
-> v druhem konstruktoru, je cout prvni -> vystup je nedef
-> v << operator je const CNUM x bez & -> vypise 3x -> 505050
-> pridam b . debug () -> void debug ( void ) const { cout << * this } -> vypise dvakrat -> 5050

-------------------------------------------------------------------------


5. -> 3. kviz

Rozdelime si na jednotlive priklady:

class A
 {
    public:
        A ( int x = 61 ) { m_X = new int (x); }
        A ( const A & src ) { m_X = new int ( *src . m_X ); }
        virtual ~A ( void ) { delete m_X; }
        virtual void print ( void ) const { cout << *m_X; }
    private:
        int *m_X;
 };
class B : public A
 {
    public:
        B ( int x, int y ) : A ( x ) { m_Y = new int (y); }
        B ( const B & src ) : A ( src ) { m_Y = new int (*src.m_Y); }
        virtual ~B ( void ) { delete m_Y; }
        virtual void print ( void ) const { A::print (); cout << *m_Y; }
        private:
            int *m_Y;
 };

void foo ( A * val )
 {
    val -> print ( );
    delete val;
 }
    int main ( void )
    {
    foo ( new B ( 81, 51 ) );
    return 0;
    } 

// virtualy oba desturktory nebo alespon virtual pro A -> tiskne 8151
// destruktory nejsou virtual nebo je destuktor jen B -> zkompiluje, ale neuvolni pamet.

class A
 {
    public:
        A ( int x ) { m_X = x; }
        virtual ~A ( void ) { }
        virtual void print ( void ) const { cout << m_X; }
    private:
        int m_X;
 };
class B : public A
 { 
    public:
        B ( int x, int y ) : A ( x ) { m_Y = y; }
        virtual void print ( void ) const { A::print (); cout << m_Y; }
        private:
        int m_Y;
 };
    void foo ( A & val )
    {
    val . print ( );
    }
int main ( void )
 {
    B test ( 39, 44 );
    foo ( test );
    return 0;
 }

// tiskne zcela spravne 3944
// misto print je ostream nebo je jen kontruktor a print ( chybi desturktor ) -> 39
// A ( int x = cislo ) { m_X = x; } + konstruktory a printy -> cout cislo
// class B:A -> nejde zkompilovat


class A
 {
    public:
        A ( int x ) { m_X = x; }
        virtual ~A ( void ) { }
        virtual void print ( ostream & os ) const { os << m_X; }
        friend ostream & operator << ( ostream & os, const A & x )
        { x . print ( os ); return os; }
    private:
        int m_X;
 };
class B : public A
 {
    public:
        B ( int x, int y ) : A ( x ) { m_Y = y; }
        virtual void print ( ostream & os ) const { A::print ( os ); os << m_Y;
}
        friend ostream & operator << ( ostream & os, const B & x )
        { os << "B:"; x . print ( os ); return os; }
    private:
        int m_Y;
 };
void foo ( A * val )
 {
    cout << *val;
 }
int main ( void )
 {
    B test ( 41, 86 );
    foo ( &test );
    return 0; 
 }

// vypise 4186
// ani jeden destrutkor neni virtual nebo virtual je jen ~B -> neuvolni pamet
// A virtual desturktor, B nema desturktor a nikde nejsou printy (jsou tam ostreamy) -> tiskne 41

class A
 {
 public:
        A ( int x ) { m_X = x; }
        virtual ~A ( void ) { }
        virtual void print ( ostream & os ) const { os << m_X; }
        friend ostream & operator << ( ostream & os, const A & x )
        { x . print ( os ); return os; }
 private:
        int m_X;
 };
class B : public A
 {
 public:
        B ( int x, int y ) : A ( x ) { m_Y = y; }
        virtual void print ( ostream & os ) const { A::print ( os ); os << m_Y;
}
        friend ostream & operator << ( ostream & os, const B & x )
        { os << "B:"; x . print ( os ); return os; }
 private:
        int m_Y;
 };
void foo ( A val )
 {
    cout << val;
 }
int main ( void )
 {
    B test ( 35, 78 );
    foo ( test );
    return 0;
 } 

// vzor + ostatni -> tiskne 35
// class B : A -> nejde zkompilovat
// A ( int x = cislo ) { m_X = x } -> tiskne cislo

class A
 {
    public:
        A ( void ) { cout << *this; }
        virtual ~A ( void ) { }
        virtual void print ( ostream & os ) const { os << "A:"; }
        friend ostream & operator << ( ostream & os, const A & x )
        { x . print ( os ); return os; }
 };
class B : public A
 {
    public:
        B ( int x ) : m_X ( x ) { cout << *this; }
        virtual void print ( ostream & os ) const { os << "B:" << m_X; }
    private:
        int m_X;
 };
int main ( void )
{
    B test ( 57 );
    return 0;
 }

// vzor + vse ostatni -> tiskne A:B:57
// print neni virtual -> nejde zkompilovat
// v konstruktoru A je cout << * this, ale v B cout << * this neni, je tam jen m_X = x -> tiskne A:
// stejne jako nahore, ale u printu A je const = 0 -> mozna spadne.
// cout << *this u obou + v B po cout << *this, mame m_X = x -> neni def.


class A
 {
 public:
         A ( int x = 55 ) { m_X = new int (x); }
        A ( const A & src ) { m_X = new int ( *src . m_X ); }
        virtual ~A ( void ) { delete m_X; }
        virtual void print ( void ) const { cout << *m_X; }
 private:
        int *m_X;
 };

class B : public A
 {
 public:
        B ( int x, int y ) : A ( x ) { m_Y = new int (y); }
        B ( const B & src ) { m_Y = new int (*src.m_Y); }
        virtual ~B ( void ) { delete m_Y; }
        virtual void print ( void ) const { A::print (); cout << *m_Y; }
 private:
        int *m_Y; 
 };
void foo ( const B & val )
 {
    val . print ( );
 }
int main ( void )
 {
    B test ( 69, 34 );
    foo ( test );
     return 0;
 } 

// vzor + vse ostatni -> tiskne 6934
// ve foo ( const B val ) -> bez & -> tiskne 5534

class A
 {
    public:
        A ( int x ) { m_X = x; }
        void print ( void ) const { cout << m_X; }
    private:
        int m_X;
 };
class B : public A
 {
    public:
        B ( int x, int y ) : A ( x ) { m_Y = y; }
        void print ( void ) const { A::print (); cout << m_Y; }
    private:
        int m_Y;
 };
void foo ( B val )
 {
    val . print ( );
 }
int main ( void )
 {
    B test ( 30, 19 );
    foo ( test );
    return 0;
 } 
// vzorove + ostatni -> 3019
// v konstruktoru B neni : A ( x ) -> nejde zkompilovat
// v konstruktoru A ( int x = cislo ) { m_X = x;} -> tiskne cislo19


class A
 {
    public:
        A ( int x ) { m_X = x; }
        virtual ~A ( void ) { }
        virtual void print ( ostream & os ) const { os << m_X; }
        friend ostream & operator << ( ostream & os, const A & x )
        { x . print ( os ); return os; }
    private:
        int m_X; 
 };
class B : public A
 {
    public:
        B ( int x, int y ) : A ( x ) { m_Y = y; }
        virtual void print ( ostream & os ) const { A::print ( os ); os << m_Y;
}
    friend ostream & operator << ( ostream & os, const B & x )
    { os << "B:"; x . print ( os ); return os; }
    private:
    int m_Y;
 };
void foo ( B * val )
 {
    cout << * val;
 }
int main ( void )
 {
     B test ( 14, 22 );
    foo ( & test );
    return 0;
 } 

 // jen jeden pripad a to B:1422

class A
 {
    public:
        A ( int x ) { m_X = x; }
        virtual ~A ( void ) { }
        virtual void print ( ostream & os ) const { os << m_X; }
        friend ostream & operator << ( ostream & os, const A & x )
        { x . print ( os ); return os; }
    private:
        int m_X;
 }; 
class B : public A
 {
    public:
        B ( int x, int y ) : A ( x ) { m_Y = y; }
        virtual void print ( ostream & os ) const { A::print ( os ); os << m_Y;
}
        friend ostream & operator << ( ostream & os, const B & x )
        { os << "B:"; x . print ( os ); return os; }
    private:
    int m_Y;
 };
void foo ( B & val )
 {
 cout << val;
 }
int main ( void )
 {
 B test ( 66, 23 );
 foo ( test );
 return 0;
 } 

 // vzor + osattni : tiskne B:6934
 // A ( int x = cislo ) { m_X = x } tiskne cislo23